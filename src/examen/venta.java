/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

/**
 *
 * @author elpep
 */
public class venta {
    private int codigoVenta;
    private int cantidad;
    private int tipoGasolina;
    private float precio;

    public venta() {
        this.codigoVenta = 0;
        this.cantidad = 0;
        this.tipoGasolina = 0;
        this.precio = 0.0f;
    }

    public venta(int codigoVenta, int cantidad, int tipoGasolina, int precio) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.tipoGasolina = tipoGasolina;
        this.precio = precio;
    }
    public venta(venta copia){
        this.codigoVenta = this.codigoVenta;
        this.cantidad = this.cantidad;
        this.precio = this.precio;
        this.tipoGasolina = this.tipoGasolina;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public float calcularCostoVenta(){
        float costo=0.0f;
        costo =  this.cantidad*this.precio;
        return costo;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularCostoVenta() * 0.16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.calcularImpuesto() + this.calcularCostoVenta();
        return total;
    
    }
   
}
