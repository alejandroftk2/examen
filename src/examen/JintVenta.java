/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

import javax.swing.JOptionPane;
 
public class JintVenta extends javax.swing.JInternalFrame {

    /**
     * Creates new form JintVenta
     */
    public JintVenta() {
        initComponents();
        this.resize(1000,400);
        this.desabilitar();
        
    }
    public void desabilitar(){
        this.txtCantidad.setEnabled(false);
        this.txtCodigo.setEnabled(false);
        this.txtCostoVent.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.txtTotalPagar.setEnabled(false);
        
        this.btnCalcular.setEnabled(false);
        this.btnGuardar.setEnabled(false);
        
    }
    public void habilitar(){
        this.txtCantidad.setEnabled(true);
        this.txtCodigo.setEnabled(true);
        
        
        this.btnGuardar.setEnabled(true);
        
    }
    public void limpiar(){
        this.txtCantidad.setText("");
        this.txtCodigo.setText("");
        this.txtCostoVent.setText("");
        this.txtImpuesto.setText("");
        this.txtPrecio.setText("");
        this.txtTotalPagar.setText("");
        
        this.cmbTipoGas.setSelectedIndex(0);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        cmbTipoGas = new javax.swing.JComboBox<>();
        btnCerrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCalcular = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtTotalPagar = new javax.swing.JTextField();
        txtCostoVent = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();

        setBackground(new java.awt.Color(119, 225, 208));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 0));
        jLabel2.setText("Codigo de venta :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 30, 180, 30);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 0));
        jLabel3.setText("Cantidad :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(60, 80, 90, 30);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 0));
        jLabel5.setText("Precio :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(90, 120, 80, 30);

        txtPrecio.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtPrecio.setForeground(new java.awt.Color(153, 0, 0));
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(170, 120, 90, 30);

        txtCantidad.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtCantidad.setForeground(new java.awt.Color(153, 0, 0));
        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });
        getContentPane().add(txtCantidad);
        txtCantidad.setBounds(170, 80, 100, 30);

        cmbTipoGas.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        cmbTipoGas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Premium ", "Regular" }));
        cmbTipoGas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoGasActionPerformed(evt);
            }
        });
        getContentPane().add(cmbTipoGas);
        cmbTipoGas.setBounds(180, 180, 100, 30);

        btnCerrar.setBackground(new java.awt.Color(51, 255, 51));
        btnCerrar.setFont(new java.awt.Font("Nirmala UI", 1, 12)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(0, 0, 204));
        btnCerrar.setText("Cerrar");
        btnCerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(660, 250, 90, 20);

        btnNuevo.setBackground(new java.awt.Color(51, 255, 51));
        btnNuevo.setFont(new java.awt.Font("Nirmala UI", 1, 12)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(0, 0, 204));
        btnNuevo.setText("Nuevo");
        btnNuevo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(770, 30, 80, 50);

        btnGuardar.setBackground(new java.awt.Color(51, 255, 51));
        btnGuardar.setFont(new java.awt.Font("Nirmala UI", 1, 12)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(0, 0, 204));
        btnGuardar.setText("Guardar");
        btnGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(770, 90, 77, 50);

        btnCalcular.setBackground(new java.awt.Color(51, 255, 51));
        btnCalcular.setFont(new java.awt.Font("Nirmala UI", 1, 12)); // NOI18N
        btnCalcular.setForeground(new java.awt.Color(0, 0, 204));
        btnCalcular.setText("Calcular");
        btnCalcular.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        getContentPane().add(btnCalcular);
        btnCalcular.setBounds(770, 150, 75, 50);

        btnLimpiar.setBackground(new java.awt.Color(51, 255, 51));
        btnLimpiar.setFont(new java.awt.Font("Nirmala UI", 1, 12)); // NOI18N
        btnLimpiar.setForeground(new java.awt.Color(0, 0, 204));
        btnLimpiar.setText("Limpiar");
        btnLimpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(370, 250, 90, 20);

        btnCancelar.setBackground(new java.awt.Color(51, 255, 51));
        btnCancelar.setFont(new java.awt.Font("Nirmala UI", 1, 12)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(0, 0, 204));
        btnCancelar.setText("Cancelar");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(510, 250, 90, 20);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos de la cotizacón", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N
        jPanel1.setForeground(new java.awt.Color(255, 255, 0));
        jPanel1.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 0));
        jLabel4.setText("Plazos :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(170, 200, 80, 30);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 0));
        jLabel6.setText("Impuesto");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(70, 80, 90, 30);

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 0));
        jLabel8.setText("Total a pagar");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(40, 130, 120, 30);

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 0));
        jLabel9.setText("Costo de venta");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(40, 30, 120, 30);

        txtTotalPagar.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtTotalPagar.setForeground(new java.awt.Color(153, 0, 0));
        txtTotalPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalPagarActionPerformed(evt);
            }
        });
        jPanel1.add(txtTotalPagar);
        txtTotalPagar.setBounds(180, 130, 90, 30);

        txtCostoVent.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtCostoVent.setForeground(new java.awt.Color(153, 0, 0));
        txtCostoVent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCostoVentActionPerformed(evt);
            }
        });
        jPanel1.add(txtCostoVent);
        txtCostoVent.setBounds(180, 30, 90, 30);

        txtImpuesto.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtImpuesto.setForeground(new java.awt.Color(153, 0, 0));
        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        jPanel1.add(txtImpuesto);
        txtImpuesto.setBounds(180, 80, 90, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(390, 20, 290, 200);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 51, 0));
        jLabel7.setText("Tipo de Gasolina");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(30, 180, 140, 30);

        txtCodigo.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        txtCodigo.setForeground(new java.awt.Color(153, 0, 0));
        txtCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigoActionPerformed(evt);
            }
        });
        getContentPane().add(txtCodigo);
        txtCodigo.setBounds(170, 30, 90, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadActionPerformed

    private void cmbTipoGasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoGasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbTipoGasActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Desea salir?", "Cotizacion", JOptionPane.YES_NO_OPTION);
        
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();
            
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        this.habilitar();
        vent = new venta();

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
     boolean ven = false;
        if(this.txtCantidad.getText().equals(""))ven = true;
        if(this.txtCodigo.getText().equals(""))ven = true;
        
        
        if (ven == true){
           
            JOptionPane.showMessageDialog(this, "falto capturar informacion");
            }
        else{
        vent.setCantidad(Integer.parseInt(this.txtCantidad.getText()));
        vent.setCodigoVenta(Integer.parseInt(this.txtCodigo.getText()));
       
        
        int tip = this.cmbTipoGas.getSelectedIndex();
        switch(tip){
                case 0:
                    vent.setTipoGasolina(0);
                    vent.setPrecio(17.50f);
                    break;
                case 1:
                    vent.setTipoGasolina(0);
                    vent.setPrecio(18.50f);
                    break;
            }
        JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
            this.btnCalcular.setEnabled(true);
        }
        

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        // TODO add your handling code here:
        //mostrar la informacion del objeto en la interfaz
        this.txtCantidad.setText(String.valueOf(vent.getCantidad()));
        this.txtCodigo.setText(String.valueOf(vent.getCodigoVenta()));
        this.txtPrecio.setText(String.valueOf(vent.getPrecio()));
        switch(vent.getTipoGasolina()){
            case 0: 
                this.cmbTipoGas.setSelectedIndex(0);
                break;
            case 1:
                this.cmbTipoGas.setSelectedIndex(0);
                break;
        }
        this.txtCostoVent.setText(String.valueOf(vent.calcularCostoVenta()));
        this.txtTotalPagar.setText(String.valueOf(vent.calcularTotalPagar()));
        this.txtImpuesto.setText(String.valueOf(vent.calcularImpuesto()));
        
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.desabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtTotalPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalPagarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalPagarActionPerformed

    private void txtCostoVentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCostoVentActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCostoVentActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void txtCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbTipoGas;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtCostoVent;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotalPagar;
    // End of variables declaration//GEN-END:variables
private venta vent;
}
